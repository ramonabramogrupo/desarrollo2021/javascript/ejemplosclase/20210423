/**
Vamos a colocar el tipo de elemento que 
tenemos entre manos
**/

let a=10; // a es un numero (dato primitivo)

let b="Ejemplo de clase" // b es un string (dato primitivo)

let c=[] // c es un array y por tanto es objetos

let d=new Date(); // d es un objeto de tipo fecha

let e=undefined; // e es un dato primitivo

let f=window; // f es objeto del BOM

let g=window.outerWidth; // g es una propiedad del objeto window y es un numero

/**
Colocamos todos los elementos en el array
**/
let todo=[a,b,c,d,e,f,g];






